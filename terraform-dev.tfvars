vpc_cidr_block = "10.0.0.0/16"

subnet_cidr_block = "10.0.10.0/24"

availability_zone = "ap-northeast-1a"

env_prefix = "dev"

local_ip = "0.0.0.0/0"

instance_type = "t2.micro"

public_key_location = "/home/ec2-user/.ssh/id_rsa.pub"
