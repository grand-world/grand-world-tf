provider "aws" {
  region = "ap-northeast-1"
}

resource "aws_vpc" "grand-world-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "grand-world-subnet" {
  vpc_id = aws_vpc.grand-world-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.availability_zone
  tags = {
    Name: "${var.env_prefix}-subnet-1"
  }
}

resource "aws_default_route_table" "grand-world-main-rtb" {
  default_route_table_id = aws_vpc.grand-world-vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.grand-world-igw.id  
  }
  tags = {
    Name: "${var.env_prefix}-main-rtb"
  }
}

resource "aws_internet_gateway" "grand-world-igw" {
  vpc_id = aws_vpc.grand-world-vpc.id
  tags = {
    Name: "${var.env_prefix}-igw"
  }
  
}

resource "aws_default_security_group" "grand-world-default-sg" {
  vpc_id = aws_vpc.grand-world-vpc.id

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["57.180.0.0/16"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name: "${var.env_prefix}-default-sg"  
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_key_pair" "grand-world-ssh-key" {
  key_name = "grand-world-ssh-key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "grand-world-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  subnet_id = aws_subnet.grand-world-subnet.id
  availability_zone = var.availability_zone
  key_name = aws_key_pair.grand-world-ssh-key.key_name  
  vpc_security_group_ids = [aws_default_security_group.grand-world-default-sg.id]
  associate_public_ip_address = true
  user_data = file("entry-script.sh")
  tags = {
    Name = "${var.env_prefix}-ec2-server"
  }
}
