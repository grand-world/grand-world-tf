output "grand-world-vpc-id" {
  value = aws_vpc.grand-world-vpc.id
}

output "grand-world-subnet-id" {
  value = aws_subnet.grand-world-subnet.id
}

output "grand-world-rtb-id" {
  value = aws_default_route_table.grand-world-main-rtb.id
}

output "grand-world-igw-id" {
  value = aws_internet_gateway.grand-world-igw.id
}

output "latest-amazon-linux-ami-id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

output "grand-world-server-public-ip" {
  value = aws_instance.grand-world-server.public_ip
}

output "grand-world-ssh-key-name" {
  value = aws_key_pair.grand-world-ssh-key.key_name
}
